/*
    This file is part of lcl.

    lcl is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lcl is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with lcl.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright (C) 2008 - 2013 Lucas Hermann Negri
*/

static int lcl_bindtextdomain(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TSTRING);
    luaL_checktype(L, 2, LUA_TSTRING);
    
    const char* app = lua_tostring(L, 1);
    const char* dir = lua_tostring(L, 2);
    
    bindtextdomain(app, dir);
    textdomain(app);
    
    return 0;
}

static int lcl_bindtextdomain_codeset(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TSTRING);
    luaL_checktype(L, 2, LUA_TSTRING);
    
    bind_textdomain_codeset(lua_tostring(L, 1), lua_tostring(L, 2));
    return 0;
}

static int lcl_gettext(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TSTRING);
    lua_pushstring(L, gettext(lua_tostring(L, 1)));
    
    return 1;
}
