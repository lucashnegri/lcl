#! /usr/bin/env lua5.1

require('lcl')

local cmp = function(a, b)
    local a = a and lcl.c_to_number(a)
    local b = b and lcl.c_to_number(b)
    return lcl.compare_string_as_number(a, b)
end

lcl.setlocale()

assert(cmp('-10', '-11') > 0)
assert(cmp('2', '11') < 0)
assert(cmp('3', '3') == 0)
assert(cmp('-100', '-100.00000000000000001') > 0)
assert(cmp('100.0', '100.00000000000000001') < 0)
assert(cmp('100', '100.00000000000000000') == 0)
assert(cmp('-100', '-100.00000000000000000') == 0)
assert(cmp('-100.00', '-100.00000000000000000') == 0)
assert(cmp('100.00000000', '100.00000000000000000') == 0)
assert(cmp('100.01', '100.00000000000000001') > 0)
assert(cmp('-100.01', '-100.00000000000000001') < 0)
assert(cmp(nil, nil) == 0)
assert(cmp(nil, '-10') < 0)
assert(cmp('10', nil) > 0)
assert(cmp('67.0000423432', '67.000042343200000000') == 0)
assert(cmp('67.000000000423432', '67.000000000323432') > 0)
assert(cmp('67.000000000423432', '67.000000000423433') < 0)
assert(cmp('67.000000000423432', '67.0000000004234321') < 0)
assert(cmp('67.0000000004234322', '67.0000000004234321333') > 0)
