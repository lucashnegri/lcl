#! /usr/bin/env lua5.1

require('lcl')

local numbers = {}
local n = 10000

local a = {
    '2324.34324324324545348768576',
    '2324.34324324',
    '2324656456456.34324324',
    '-1',
    '-1.2',
    '-1.00002',
    '-2',
    '0',
    '2309',
    '2309',
    '98',
    '6879',
    '1',
    '2',
    '79823',
    '8427'
}


for i = 1, n do
    for _, j in pairs(a) do
        numbers[#numbers + 1] = j
    end
end

local start = os.clock()
table.sort(numbers, function(a, b) return tonumber(a) < tonumber(b) end)
print('Sort 1: ', os.clock() - start)

numbers = {}

for i = 1, n do
    for _, j in pairs(a) do
        numbers[#numbers + 1] = j
    end
end

local start = os.clock()
local cmp = lcl.compare_string_as_number
table.sort(numbers, function(a, b) return cmp(a, b) < 0 end)
print('Sort 2: ', os.clock() - start)
