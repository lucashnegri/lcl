# Compiler and flags
CC=gcc
PKG_PACKAGES=lua5.1
CFLAGS=-pipe -Wall -ansi `pkg-config --cflags $(PKG_PACKAGES)` -fPIC
LDFLAGS=-shared `pkg-config --libs $(PKG_PACKAGES)`

# Options
ifdef DEBUG
	CFLAGS+=-g -O0 -DIDEBUG
else
	CFLAGS+=-O2
endif

# Platform dependent
RM=rm -rf
CP=cp
MKDIR=mkdir -p
DESTDIR = /
PREFIX  = /usr
NAME=_lcl.so

# Rules

.PHONY: all
all: $(NAME)
	
$(NAME): src/gettext.c src/lcl.c src/lcl.h src/locale.c
	$(CC) $(CFLAGS) src/lcl.c -o $(NAME) $(LDFLAGS)

.PHONY: install	
install: all
	$(MKDIR) $(DESTDIR)/$(PREFIX)/lib/lua/5.1
	$(MKDIR) $(DESTDIR)/$(PREFIX)/share/lua/5.1
	$(CP) $(NAME) $(DESTDIR)/$(PREFIX)/lib/lua/5.1
	$(CP) src/lcl.lua $(DESTDIR)/$(PREFIX)/share/lua/5.1/lcl.lua

.PHONY: clean
clean:
	$(RM) $(NAME)
	
.PHONY: help
help:
	@echo Options:
	@echo
	@echo DEBUG=1 to enable debug mode
