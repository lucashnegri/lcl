lcl
===

About
-----

lcl is a i18n and l10n library for Lua, that wraps GNU gettext and ANSI locale.h.
There are packages for Ubuntu 13.10 on *ppa:lucashnegri/lua*.

Dependencies:
-------------

* gettext;
* libc6-dev;
* Lua 5.1.x, with development files;

Install
-------

    $ make [DEBUG=1]
    # make install

Contact
-------

Lucas Hermann Negri - <lucashnegri@gmail.com>
http://oproj.tuxfamily.org
