#! /usr/bin/env lua5.1

require('lcl')

lcl.setlocale()

-- Number conversion

local ep = 10e-9
local aux

for i = 1, 1000, 1.33 do
    aux = tonumber(lcl.c_to_number(lcl.number_to_c(tostring(i))))
    assert(math.abs(i - aux) < ep)
end

-- Date

local today

for i = 1, 1000 do
    today = os.date('%Y/%m/%d')
    assert(lcl.c_to_date(lcl.parse_date(today)) == os.date('%x'))
end
