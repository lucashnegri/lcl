/*
    This file is part of lcl.

    lcl is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lcl is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with lcl.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright (C) 2008 - 2013 Lucas Hermann Negri
*/

#include "lcl.h"
#include "gettext.c"
#include "locale.c"

/**
 * Returns the current version of the library, usually 
 * major.minor.bugfix, ex.: 0.4.2
 */
static int lcl_version(lua_State* L)
{
    lua_pushstring(L, LIB_VERSION);
    return 1;
}

static const struct luaL_reg lcl [] =
{
    {"version", lcl_version},
    {"bindtextdomain", lcl_bindtextdomain},
    {"bindtextdomain_codeset", lcl_bindtextdomain_codeset},
    {"gettext", lcl_gettext},
    {"setlocale", lcl_setlocale},
    {"getfield", lcl_getfield},
    {"compare_string_as_number", lcl_compare_string_as_number},
    {NULL, NULL}
};

int luaopen__lcl(lua_State *L)
{
    /* Register the basic functions */
    luaL_register(L, LIB_NAME, lcl);
    
    /* Initialize the locale struct */
    init_locale();
    
    /* Push the enums */
    
    lua_pushliteral(L, "NUMERIC"); lua_pushnumber(L, LC_NUMERIC); lua_rawset(L, -3);
    lua_pushliteral(L, "MONETARY"); lua_pushnumber(L, LC_MONETARY); lua_rawset(L, -3);
    lua_pushliteral(L, "ALL"); lua_pushnumber(L, LC_ALL); lua_rawset(L, -3);
    
    lua_pushliteral(L, "CURRENCY_SYMBOL"); lua_pushnumber(L, CURRENCY_SYMBOL); lua_rawset(L, -3);
    lua_pushliteral(L, "DECIMAL_POINT"); lua_pushnumber(L, DECIMAL_POINT); lua_rawset(L, -3);
    lua_pushliteral(L, "FRAC_DIGITS"); lua_pushnumber(L, FRAC_DIGITS); lua_rawset(L, -3);
    lua_pushliteral(L, "GROUPING"); lua_pushnumber(L, GROUPING); lua_rawset(L, -3);
    lua_pushliteral(L, "INT_CURR_SYMBOL"); lua_pushnumber(L, INT_CURR_SYMBOL); lua_rawset(L, -3);
    lua_pushliteral(L, "INT_FRAC_DIGITS"); lua_pushnumber(L, INT_FRAC_DIGITS); lua_rawset(L, -3);
    lua_pushliteral(L, "MON_DECIMAL_POINT"); lua_pushnumber(L, MON_DECIMAL_POINT); lua_rawset(L, -3);
    lua_pushliteral(L, "MON_THOUSANDS_SEP"); lua_pushnumber(L, MON_THOUSANDS_SEP); lua_rawset(L, -3);
    lua_pushliteral(L, "MON_GROUPING"); lua_pushnumber(L, MON_GROUPING); lua_rawset(L, -3);
    lua_pushliteral(L, "NEGATIVE_SIGN"); lua_pushnumber(L, NEGATIVE_SIGN); lua_rawset(L, -3);
    lua_pushliteral(L, "N_CS_PRECEDES"); lua_pushnumber(L, N_CS_PRECEDES); lua_rawset(L, -3);
    lua_pushliteral(L, "N_SEP_BY_SPACE"); lua_pushnumber(L, N_SEP_BY_SPACE); lua_rawset(L, -3);
    lua_pushliteral(L, "N_SIGN_POSN"); lua_pushnumber(L, N_SIGN_POSN); lua_rawset(L, -3);
    lua_pushliteral(L, "POSITIVE_SIGN"); lua_pushnumber(L, POSITIVE_SIGN); lua_rawset(L, -3);
    lua_pushliteral(L, "P_CS_PRECEDES"); lua_pushnumber(L, P_CS_PRECEDES); lua_rawset(L, -3);
    lua_pushliteral(L, "P_SEP_BY_SPACE"); lua_pushnumber(L, P_SEP_BY_SPACE); lua_rawset(L, -3);
    lua_pushliteral(L, "P_SIGN_POSN"); lua_pushnumber(L, P_SIGN_POSN); lua_rawset(L, -3);
    lua_pushliteral(L, "THOUSANDS_SEP"); lua_pushnumber(L, THOUSANDS_SEP); lua_rawset(L, -3);

    return 1;
}
