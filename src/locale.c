/*
    This file is part of lcl.

    lcl is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lcl is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with lcl.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright (C) 2008 - 2013 Lucas Hermann Negri
*/

enum Field
{
    CURRENCY_SYMBOL = 1, DECIMAL_POINT, FRAC_DIGITS, GROUPING, INT_CURR_SYMBOL,
    INT_FRAC_DIGITS, MON_DECIMAL_POINT, MON_GROUPING, MON_THOUSANDS_SEP, NEGATIVE_SIGN,
    N_CS_PRECEDES, N_SEP_BY_SPACE, N_SIGN_POSN, POSITIVE_SIGN, P_CS_PRECEDES,
    P_SEP_BY_SPACE, P_SIGN_POSN, THOUSANDS_SEP
};

static struct lconv *locale = NULL;

static void ret_ch(lua_State* L, const char ch)
{
    char b[1];
    b[0] = ch;
    lua_pushlstring(L, b, 1);
}

static void init_locale()
{
    locale = localeconv();
}

static int lcl_setlocale(lua_State* L)
{
    int top = lua_gettop(L), type;
    char* name;
    
    if(top > 1)
    {
        luaL_checktype(L, 1, LUA_TNUMBER);
        luaL_checktype(L, 2, LUA_TSTRING);
        type = lua_tointeger(L, 1);
        name = (char*)lua_tostring(L, 2);
    }
    else
    {
        type = LC_ALL;
        name = "";
    }
    
    char* res = setlocale(type, name);
    lua_pushstring(L, res);
    
    /* Get the current locale */
    locale = localeconv();
    
    return 1;
}

static int lcl_getfield(lua_State* L)
{   
    int top = lua_gettop(L), i;
    
    for(i = 1; i <= top; ++i)
    {
        int field = luaL_checkinteger(L, i);
        
        switch(field)
        {
            case CURRENCY_SYMBOL:       lua_pushstring(L, locale->currency_symbol); break;
            case DECIMAL_POINT:         lua_pushstring(L, locale->decimal_point);   break;
            case FRAC_DIGITS:           ret_ch(L, locale->frac_digits);     break;
            case GROUPING:              lua_pushstring(L, locale->grouping);        break;
            case INT_CURR_SYMBOL:       lua_pushstring(L, locale->int_curr_symbol); break;
            case INT_FRAC_DIGITS:       ret_ch(L, locale->int_frac_digits); break;
            case MON_DECIMAL_POINT:     lua_pushstring(L, locale->mon_decimal_point); break;
            case MON_GROUPING:          lua_pushstring(L, locale->mon_grouping);    break;
            case MON_THOUSANDS_SEP:     lua_pushstring(L, locale->mon_thousands_sep); break;
            case NEGATIVE_SIGN:         lua_pushstring(L, locale->negative_sign);   break;
            case N_CS_PRECEDES:         ret_ch(L, locale->n_cs_precedes);   break;
            case N_SEP_BY_SPACE:        ret_ch(L, locale->n_sep_by_space);  break;
            case N_SIGN_POSN:           ret_ch(L, locale->n_sign_posn);     break;
            case POSITIVE_SIGN:         lua_pushstring(L, locale->positive_sign);   break;
            case P_CS_PRECEDES:         ret_ch(L, locale->p_cs_precedes);   break;
            case P_SEP_BY_SPACE:        ret_ch(L, locale->p_sep_by_space);  break;
            case P_SIGN_POSN:           ret_ch(L, locale->p_sign_posn);     break;
            case THOUSANDS_SEP:         lua_pushstring(L, locale->thousands_sep);   break;
            default: lua_pushnil(L);
        }
    }
    
    return top;
}

static int lcl_compare_string_as_number(lua_State* L)
{
    /* Preparations */
    size_t len1, len2;
    const char* str1 = lua_tolstring(L, 1, &len1);
    const char* str2 = lua_tolstring(L, 2, &len2);
    char dec = locale->decimal_point[0]; if(!dec) dec = '.';
    int res = 0, aux, min, plus1 = 1, plus2 = 1;
    
    /* Check for nulls */
    if(!len1 || !len2)
    { 
        if(!len1) --res;
        if(!len2) ++res;
        goto lend;
    }
    
    char neg = locale->negative_sign[0]; if(!neg) neg = '-';
    if(*str1 == neg) {plus1 = 0; ++str1; --len1;};
    if(*str2 == neg) {plus2 = 0; ++str2; --len2;};
    
    /* Skip trailing zeroes */
    while(*str1 == '0') {++str1, --len1;}; while(*str2 == '0') {++str2, --len2;};
    
    int num1 = len1, num2 = len2;
    
    /* Get the number of digits of the integer part of the numbers */
    for(aux = 0; aux < len1; ++aux) if(str1[aux] == dec) {num1 = aux; break;}
    for(aux = 0; aux < len2; ++aux) if(str2[aux] == dec) {num2 = aux; break;}
    
    /* Compare the digits */
    if(num1 != num2) {res = num1 - num2; goto lend;}
    
    min = len1 < len2 ? len1 : len2;
    
    /* Compare the decimal digits */
    for(aux = 0; aux < min; ++aux)
        if(str1[aux] != str2[aux]) {res = str1[aux] - str2[aux]; goto lend;}
    
    /* Skip the separator */
    if(str1[min] == dec) ++str1; if(str2[min] == dec) ++str2;
    
    /* At least one of the strings is at the end, so only one loop will run */
    for(aux = min; aux < len1; ++aux) if(str1[aux] && str1[aux] != '0') {++res; break;}
    for(aux = min; aux < len2; ++aux) if(str2[aux] && str2[aux] != '0') {--res; break;}
    
    lend:
    
    /* Handle negative numbers */
    if(!plus1)
        if(plus2)
            {res = -1;}
        else
            {res = -res;}
    else
        if(!plus2) res = 1;
    
    lua_pushinteger(L, res);
    return 1;
}
