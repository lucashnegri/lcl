/*
    This file is part of lcl.

    lcl is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lcl is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with lcl.  If not, see <http://www.gnu.org/licenses/>.
    
    Copyright (C) 2008 - 2013 Lucas Hermann Negri
*/

#ifndef LCL_LCL
#define LCL_LCL

#define LIB_NAME "lcl"
#define LIB_VERSION "1.1"

/* Lua headers */
#include <lua.h>
#include <lauxlib.h>

/* C headers */
#include <locale.h>
#include <libintl.h>
#include <string.h>

#endif
